<?php

/***************************************************************************\
 *  SPIP, Système de publication pour l'internet                           *
 *                                                                         *
 *  Copyright © avec tendresse depuis 2001                                 *
 *  Arnaud Martin, Antoine Pitrou, Philippe Rivière, Emmanuel Saint-James  *
 *                                                                         *
 *  Ce programme est un logiciel libre distribué sous licence GNU/GPL.     *
\***************************************************************************/

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
include_spip('inc/dump');

/**
 * Charger #FORMULAIRE_IMPORTER_DROPBOX
 *
 * @return array
 */
function formulaires_importer_dropbox_charger_dist() {

	$valeurs = [
		'_dir_dump' => dump_repertoire(),
		'choisi' => _request('fichier'),		
		'fichier' => '',
		'tri' => 'nom',
	];

	return $valeurs;
}

/**
 * Verifier
 *
 * @return array
 */
function formulaires_importer_dropbox_verifier_dist() {
	$erreurs = [];
	if (!$fichier = _request('fichier')) {
		$erreurs['fichier'] = _T('info_obligatoire');
	} else {
		$fichier = basename($fichier); // securite
	}

	if ($fichier and !count($erreurs)) {
		if (_request('confirm') !== $fichier) {
			$erreurs['message_confirm'] =
				_T(
					'dropbox:info_selection_import',
					['fichier' => '<i>' . joli_repertoire(dump_repertoire() . $fichier) . '</i>']
				)
				. "<br /><input type='checkbox' name='confirm' value='$fichier' id='confirm' /> ";
			$erreurs['message_confirm'] .= "<label for='confirm'><strong>";
			$erreurs['message_confirm'] .= _T('dropbox:confirmer_importer_base');
			$erreurs['message_confirm'] .= '</strong></label>';
		} 
	}

	if (count($erreurs) and !isset($erreurs['message_erreur'])) {
		$erreurs['message_erreur'] = '';
	} // pas de message general automatique ici
	return $erreurs;
}

/**
 * Traiter
 *
 * @return array
 */
function formulaires_importer_dropbox_traiter_dist() {

	$fichier = _request('fichier');
	
	include_spip('inc/dump');
	$dir_dump = dump_repertoire();

	// on lance l'import
	$recup=dropbox_api_download($dir_dump,$fichier);
	
	if ($recup['statut'] == '200') {
		$res['message_ok'] = _T('dropbox:info_import_ok',['fichier' => '<i>'.joli_repertoire($dir_dump. $fichier).'</i>'])
				.'<script type="text/javascript">if (window.jQuery) ajaxReload("inc-lister-fichiers");</script>';
	} else {
		$res['message_erreur'] = $recup['statut'].' '.$recup['erreur'];
	}
		
	return $res;
}
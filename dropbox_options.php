<?php

/**
 * Options au chargement du plugin Dropbox
 *
 * @plugin     Dropbox
 * @copyright  2023
 * @author     JMarc_64
 * @licence    GNU/GPL
 * @package    SPIP\Dropbox\Options
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/*
 * Un fichier d'options permet de définir des éléments
 * systématiquement chargés à chaque hit sur SPIP.
 *
 * Il vaut donc mieux limiter au maximum son usage
 * tout comme son volume !
 *
 */
 
 
 /*
 * L'appel de la fonction "dump_lister_sauvegardes" du plugin dump de la dist 
 * dans inc-lister-fichiers provoque une erreur "Call to undefined function"
 * => Chargement de la fonction par défaut
 */
include_spip('inc/dump');
 

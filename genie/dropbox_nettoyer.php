<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
	
/**
 * Nettoyage journalier des exports Dropbox obsolètes.
 *
 * @param timestamp $last
 *
 * @return int
 */
function genie_dropbox_nettoyer($last) {
	// Lancement du nettoyage journalier des exports
	include_spip('dropbox_fonctions');
	dropbox_nettoyer(['auteur' => 'cron']);
	return 1;
}

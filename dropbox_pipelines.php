<?php

/**
 * Utilisations de pipelines par Dropbox
 *
 * @plugin     Dropbox
 * @copyright  2023
 * @author     JMarc_64
 * @licence    GNU/GPL
 * @package    SPIP\Dropbox\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * On s'insère dans le pipeline post_sauvegarde des plugin Saveauto et/ou Mes_fichiers et/ou Dumpauto
 * On exporte les sauvegardes en fonction du paramétrage du plugin
 */
function dropbox_post_sauvegarde($flux) {

	// on vérifie le paramétrage d'export automatique des sauvegardes Saveauto
	// et la présence des arguments du pipeline

	if (lire_config('dropbox/export_saveauto')
	AND isset($flux['args']['type']) 
	AND (($flux['args']['type'])=='saveauto')
	AND (empty($flux['args']['err']))
	) {
		// include des fonctions Dropbox pour le cron
		include_spip('dropbox_fonctions');

		// récupération des chemins et nom de la sauvegarde Saveauto
		$chemin_fichier=$flux['args']['chemin_fichier'];
		$nom_fichier=$flux['args']['nom_fichier'];
		
		// Extraire seulement le path avec un slash à la fin
		$chemin_fichier = dirname($chemin_fichier).'/';
				
		// Vérification de l'existance du fichier
		if (!file_exists($chemin_fichier . $nom_fichier)){
			$message = _T('dropbox:info_aucun_fichier_trouve').': '.joli_repertoire($chemin_fichier.$nom_fichier);
			spip_log(_T('dropbox:info_aucun_fichier_trouve').' '.joli_repertoire($chemin_fichier.$nom_fichier),_LOG_ERREUR);			
		} 
		// Vérification de la taille maximum du fichier exporté  (150 Mo)
		elseif (filesize($chemin_fichier.$nom_fichier) > 150*1024*1024 )
		{
			$message = $nom_fichier.' - '._T('dropbox:info_taille_max_fichier').': '.taille_en_octets(filesize($chemin_fichier.$nom_fichier));
			spip_log($nom_fichier.' - '._T('dropbox:info_taille_max_fichier').': '.taille_en_octets(filesize($chemin_fichier.$nom_fichier)),_LOG_ERREUR);			
		}
		else
		// Export du fichier
		{
			$recup=dropbox_api_upload($chemin_fichier,$nom_fichier);
		}
		
		// Construction du message en fonction du statut en retour s'il n'y a pas eu de message (d'erreur) auparavant
		if (!isset($message)){
			if ($recup['statut'] == '200') {
				$message = _T('dropbox:info_export_ok',['fichier' => joli_repertoire($chemin_fichier. $nom_fichier)]);
				spip_log(_T('dropbox:info_export_ok',['fichier' => joli_repertoire($chemin_fichier. $nom_fichier)]),_LOG_INFO);
			} else {
				$message = _T('dropbox:info_export_ko',['fichier' => joli_repertoire($chemin_fichier. $nom_fichier)]).' Statut = '.$recup['statut'].' Erreur= '.$recup['erreur'];
				spip_log(_T('dropbox:info_export_ko',['fichier' => joli_repertoire($chemin_fichier. $nom_fichier)]).' Statut = '.$recup['statut'].' Erreur= '.$recup['erreur'],_LOG_ERREUR);
			}
		}
			
		// On envoie une notification si demandé dans la configuration
		if(lire_config('dropbox/notif_saveauto')){
			
			// Construction du sujet du mail
			include_spip('inc/texte');
			$base = $GLOBALS['connexions'][0]['db'];
			$sujet = "[" . typo($GLOBALS['meta']['nom_site'])
					. "][Dropbox] "
					. _T('dropbox:message_export_sujet', array('base' => $base));
						
			// Détermination de la liste des destinataires à partir du paramétrage du plugin Saveauto
			$mails = lire_config('saveauto/notif_mail');
			$tous = ($mails) ? explode(',', $mails) : array();
			if (lire_config('saveauto/notification_webmestre') == 1)
				$tous[] = $GLOBALS['meta']['email_webmaster'];
			
			// Pasage par le pipeline "notifications_destinataires" (même si interêt pas évident...à creuser!)
			$destinataires = pipeline('notifications_destinataires',
						array(
							'args'=>array('quoi'=>'dropbox','id'=>'','options'=>''),
							'data'=>$tous)
							);
							
			// Envoi des emails
			$envoyer_mail = charger_fonction('envoyer_mail','inc');
			foreach($destinataires as $email){
				$envoyer_mail($email, $sujet, $message, '', '');
			}
		}
	

	}
	
	// on vérifie le paramétrage d'export automatique des sauvegardes Mes_fichiers
	// et la présence des arguments du pipeline

	if (lire_config('dropbox/export_mes_fichiers')
	AND isset($flux['args']['type']) 
	AND (($flux['args']['type'])=='mes_fichiers_sauver')
	AND (empty($flux['args']['err']))
	) {
		// include des fonctions Dropbox pour le cron
		include_spip('dropbox_fonctions');

		// récupération du chemin de la sauvegarde Mes_fichiers
		$archive=$flux['args']['archive'];
		
		// Extraire seulement le path avec un slash à la fin
		$chemin_fichier = dirname($archive).'/';
		
		// Extraire seulement le nom du fichier
		$nom_fichier = basename($archive);
				
		// Vérification de l'existance du fichier
		if (!file_exists($chemin_fichier . $nom_fichier)){
			$message = _T('dropbox:info_aucun_fichier_trouve').': '.joli_repertoire($chemin_fichier.$nom_fichier);
			spip_log(_T('dropbox:info_aucun_fichier_trouve').' '.joli_repertoire($chemin_fichier.$nom_fichier),_LOG_ERREUR);			
		} 
		// Vérification de la taille maximum du fichier exporté  (150 Mo)
		elseif (filesize($chemin_fichier.$nom_fichier) > 150*1024*1024 )
		{
			$message = $nom_fichier.' - '._T('dropbox:info_taille_max_fichier').': '.taille_en_octets(filesize($chemin_fichier.$nom_fichier));
			spip_log($nom_fichier.' - '._T('dropbox:info_taille_max_fichier').': '.taille_en_octets(filesize($chemin_fichier.$nom_fichier)),_LOG_ERREUR);			
		}
		else
		// Export du fichier
		{
			$recup=dropbox_api_upload($chemin_fichier,$nom_fichier);
		}
		
		// Construction du message en fonction du statut en retour s'il n'y a pas eu de message (d'erreur) auparavant
		if (!isset($message)){
			if ($recup['statut'] == '200') {
				$message = _T('dropbox:info_export_ok',['fichier' => joli_repertoire($chemin_fichier. $nom_fichier)]);
				spip_log(_T('dropbox:info_export_ok',['fichier' => joli_repertoire($chemin_fichier. $nom_fichier)]),_LOG_INFO);
			} else {
				$message = _T('dropbox:info_export_ko',['fichier' => joli_repertoire($chemin_fichier. $nom_fichier)]).' Statut = '.$recup['statut'].' Erreur= '.$recup['erreur'];
				spip_log(_T('dropbox:info_export_ko',['fichier' => joli_repertoire($chemin_fichier. $nom_fichier)]).' Statut = '.$recup['statut'].' Erreur= '.$recup['erreur'],_LOG_ERREUR);
			}
		}
			
		// On envoie une notification si demandé dans la configuration
		if(lire_config('dropbox/notif_mes_fichiers')){
			
			// Construction du sujet du mail
			include_spip('inc/texte');
			$base = $GLOBALS['connexions'][0]['db'];
			$sujet = "[" . typo($GLOBALS['meta']['nom_site'])
					. "][Dropbox] "
					. _T('dropbox:message_export_sujet', array('base' => $base));
						
			// Détermination de la liste des destinataires à partir du paramétrage du plugin Mes_fichiers
			$mails = lire_config('mes_fichiers/notif_mail');
			$tous = ($mails) ? explode(',', $mails) : array();
			// Ajout systématique du webmaster
			$tous[] = $GLOBALS['meta']['email_webmaster'];
			
			// Pasage par le pipeline "notifications_destinataires"
			$destinataires = pipeline('notifications_destinataires',
						array(
							'args'=>array('quoi'=>'dropbox','id'=>'','options'=>''),
							'data'=>$tous)
							);
							
			// Envoi des emails
			$envoyer_mail = charger_fonction('envoyer_mail','inc');
			foreach($destinataires as $email){
				$envoyer_mail($email, $sujet, $message, '', '');
			}
		}
	

	}
	
	// on vérifie le paramétrage d'export automatique des sauvegardes Dumpauto
	// et la présence des arguments du pipeline

	if (lire_config('dropbox/export_dumpauto')
	AND isset($flux['args']['type']) 
	AND (($flux['args']['type'])=='dumpauto_sauver')
	) {
		// include des fonctions Dropbox pour le cron
		include_spip('dropbox_fonctions');

		// récupération du chemin de la sauvegarde Dumpauto
		$archive=$flux['args']['archive'];
		
		// Extraire seulement le path avec un slash à la fin
		$chemin_fichier = dirname($archive).'/';
		
		// Extraire seulement le nom du fichier
		$nom_fichier = basename($archive);
				
		// Vérification de l'existance du fichier
		if (!file_exists($chemin_fichier . $nom_fichier)){
			$message = _T('dropbox:info_aucun_fichier_trouve').': '.joli_repertoire($chemin_fichier.$nom_fichier);
			spip_log(_T('dropbox:info_aucun_fichier_trouve').' '.joli_repertoire($chemin_fichier.$nom_fichier),_LOG_ERREUR);			
		} 
		// Vérification de la taille maximum du fichier exporté  (150 Mo)
		elseif (filesize($chemin_fichier.$nom_fichier) > 150*1024*1024 )
		{
			$message = $nom_fichier.' - '._T('dropbox:info_taille_max_fichier').': '.taille_en_octets(filesize($chemin_fichier.$nom_fichier));
			spip_log($nom_fichier.' - '._T('dropbox:info_taille_max_fichier').': '.taille_en_octets(filesize($chemin_fichier.$nom_fichier)),_LOG_ERREUR);			
		}
		else
		// Export du fichier
		{
			$recup=dropbox_api_upload($chemin_fichier,$nom_fichier);
		}
		
		// Construction du message en fonction du statut en retour s'il n'y a pas eu de message (d'erreur) auparavant
		if (!isset($message)){
			if ($recup['statut'] == '200') {
				$message = _T('dropbox:info_export_ok',['fichier' => joli_repertoire($chemin_fichier. $nom_fichier)]);
				spip_log(_T('dropbox:info_export_ok',['fichier' => joli_repertoire($chemin_fichier. $nom_fichier)]),_LOG_INFO);
			} else {
				$message = _T('dropbox:info_export_ko',['fichier' => joli_repertoire($chemin_fichier. $nom_fichier)]).' Statut = '.$recup['statut'].' Erreur= '.$recup['erreur'];
				spip_log(_T('dropbox:info_export_ko',['fichier' => joli_repertoire($chemin_fichier. $nom_fichier)]).' Statut = '.$recup['statut'].' Erreur= '.$recup['erreur'],_LOG_ERREUR);
			}
		}
			
		// On envoie une notification si demandé dans la configuration
		if(lire_config('dropbox/notif_dumpauto')){
			
			// Construction du sujet du mail
			include_spip('inc/texte');
			$base = $GLOBALS['connexions'][0]['db'];
			$sujet = "[" . typo($GLOBALS['meta']['nom_site'])
					. "][Dropbox] "
					. _T('dropbox:message_export_sujet', array('base' => $base));
						
			// Détermination de la liste des destinataires à partir du paramétrage du plugin Dumpauto
			$mails = lire_config('dumpauto/mails');
			$tous = ($mails) ? explode(',', $mails) : array();
			// Ajout systématique du webmaster
			$tous[] = $GLOBALS['meta']['email_webmaster'];
			
			// Pasage par le pipeline "notifications_destinataires"
			$destinataires = pipeline('notifications_destinataires',
						array(
							'args'=>array('quoi'=>'dumpauto','id'=>'','options'=>''),
							'data'=>$tous)
							);
							
			// Envoi des emails
			$envoyer_mail = charger_fonction('envoyer_mail','inc');
			foreach($destinataires as $email){
				$envoyer_mail($email, $sujet, $message, '', '');
			}
		}
	

	}
}
function dropbox_taches_generales_cron($taches_generales) {

	include_spip('inc/config');
	if (lire_config('dropbox/nettoyage')) {
		// Nettoyage journalier des exports obsolètes
		$duree = lire_config('dropbox/duree',15);
		if(is_numeric($duree) && $duree > 0) {
		$taches_generales['dropbox_nettoyer'] = 24 * 3600;
		}
	}
		
return $taches_generales;

}
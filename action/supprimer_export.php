<?php

/**
 * Fonctions utiles au plugin Dropbox
 *
 * @plugin     Dropbox
 * @copyright  2023
 * @author     JMarc_64
 * @licence    GNU/GPL
 * @package    SPIP\Dropbox\Fonctions
 *
 * Action sécurisée supprimer export Dropbox
 *
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/dump');
include_spip('inc/autoriser');
include_spip('dropbox_fonctions');

/**
 * Supprimer un export Dropbox quand on est webmestre
 *
 * @param string $arg
 */
function action_supprimer_export_dist($arg = null) {
	
	if (!$arg) {
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$arg = $securiser_action();
	}

	$fichier = $arg;
	
	$dir_dump = dump_repertoire();

	if (autoriser('webmestre')) {
		// appel de API de delete de fichier
		$res=dropbox_api_delete($dir_dump,$fichier);
	}
}

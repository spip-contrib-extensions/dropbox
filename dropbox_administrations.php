<?php

/**
 * Fichier gérant l'installation et désinstallation du plugin Dropbox
 *
 * @plugin     Dropbox
 * @copyright  2023
 * @author     JMarc_64
 * @licence    GNU/GPL
 * @package    SPIP\Dropbox\Installation
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Fonction d'installation et de mise à jour du plugin Dropbox.
 *
 * Vous pouvez :
 *
 * - créer la structure SQL,
 * - insérer du pre-contenu,
 * - installer des valeurs de configuration,
 * - mettre à jour la structure SQL 
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @param string $version_cible
 *     Version du schéma de données dans ce plugin (déclaré dans paquet.xml)
 * @return void
**/
function dropbox_upgrade($nom_meta_base_version, $version_cible) {
	$maj = [];
	# quelques exemples
	# (que vous pouvez supprimer !)
	#
	# $maj['create'] = [['creer_base']];
	#
	# include_spip('inc/config')
	# $maj['create'] = [
	#	['maj_tables', ['spip_xx', 'spip_xx_liens']],
	#	['ecrire_config', 'dropbox', ['exemple' => "Texte de l'exemple"]],
	#];
	#
	# $maj['1.1.0']  = [['sql_alter','TABLE spip_xx RENAME TO spip_yy']];
	# $maj['1.2.0']  = [['sql_alter','TABLE spip_xx DROP COLUMN id_auteur']];
	# $maj['1.3.0']  = [
	#	['sql_alter','TABLE spip_xx CHANGE numero numero int(11) default 0 NOT NULL'],
	#	['sql_alter','TABLE spip_xx CHANGE texte petit_texte mediumtext NOT NULL default \'\''],
	# ];
	# ...

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}


/**
 * Fonction de désinstallation du plugin Dropbox.
 *
 * Vous devez :
 *
 * - nettoyer toutes les données ajoutées par le plugin et son utilisation
 * - supprimer les tables et les champs créés par le plugin. 
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @return void
**/
function dropbox_vider_tables($nom_meta_base_version) {
	# quelques exemples
	# (que vous pouvez supprimer !)
	# sql_drop_table('spip_xx');
	# sql_drop_table('spip_xx_liens');


	effacer_meta($nom_meta_base_version);
}

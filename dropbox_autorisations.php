<?php

/**
 * Définit les autorisations du plugin Dropbox
 *
 * @plugin     Dropbox
 * @copyright  2023
 * @author     JMarc_64
 * @licence    GNU/GPL
 * @package    SPIP\Dropbox\Autorisations
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/*
 * Un fichier d'autorisations permet de regrouper
 * les fonctions d'autorisations de votre plugin
 */

/**
 * Fonction d'appel pour le pipeline
 * @pipeline autoriser */
function dropbox_autoriser() {
}

function autoriser_dropbox_configurer_dist($faire, $type, $id, $qui, $opt) {
	return autoriser('sauvegarder', $type, $id, $qui, $opt);
}

function autoriser_dropbox_menu_dist($faire, $type, $id, $qui, $opt) {
	return autoriser('sauvegarder', $type, $id, $qui, $opt);
}

function autoriser_dropboxexporter_onglet_dist($faire, $type, $id, $qui, $opt) {
	return autoriser('sauvegarder', $type, $id, $qui, $opt);
}

function autoriser_dropboximporter_onglet_dist($faire, $type, $id, $qui, $opt) {
	return autoriser('sauvegarder', $type, $id, $qui, $opt);
}

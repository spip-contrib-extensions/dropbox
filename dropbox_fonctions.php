
<?php

/**
 * Fonctions utiles au plugin Dropbox
 *
 * @plugin     Dropbox
 * @copyright  2023
 * @author     JMarc_64
 * @licence    GNU/GPL
 * @package    SPIP\Dropbox\Fonctions
 *
 * Autorisation, token, filtres et API Dropbox
 *
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function dropbox_test_app() {
	
	$res['statut'] = null;
	
	// test la validité des client_id et client_secret  
	$res=dropbox_api_check_app();
	
	return $res['statut'];

}

function dropbox_liste_exports($action) {
	// Liste des fichiers sur Dropbox
		
	include_spip('inc/dump');
	$dump_repertoire=dump_repertoire();
	$path='/'.joli_repertoire($dump_repertoire);
	
	$res=dropbox_api_list_folder($action,$path);

	// Sauvegarde du statut et erreurs en session
	session_set('dropbox_statut', $res['statut']);
	
	if ($res['erreur']!='200'){
		session_set('dropbox_erreur', $res['erreur']);
	}
	
	return $res['resultat'];
	
}

function dropbox_api_check_app() {
	
	// Check des client_id et client_secret  
	
	$client_id = lire_config('dropbox/client_id');
	$client_secret = lire_config('dropbox/client_secret');
	$basic = base64_encode ($client_id.':'.$client_secret);
	
	$url = 'https://api.dropboxapi.com/2/check/app';
	
	$headers = array (	'Authorization: Basic '.$basic,
						'Content-Type: application/json',
					);
	$datas =  json_encode (array(
					'query' => 'foo',
					)
				);
					
	$ch = curl_init($url);
	
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $datas);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_VERBOSE, 1); // debug

	$recup = curl_exec($ch);

	curl_close($ch);    
			
	$res['statut'] = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	
	if ($res['statut'] != '200'){
		$res['erreur'] = $recup;
		$res ['resultat'] = null;
		return $res;
	}
		
	$res['resultat']=$recup;
	
	return $res;
}

function dropbox_api_download($repertoire,$fichier) {
	
	// API: Download un fichier Dropbox
	
	$action = 'importer';
	$res = dropbox_demander_token($action);
	
	if ($res['statut']!='200'){
		$res ['resultat'] = null;
		return $res;
	}
	
	$token = $res['token'];
		
	$nom_fichier=$repertoire.$fichier;
		
	$url = 'https://content.dropboxapi.com/2/files/download';
	
	$headers = array (	'Authorization: Bearer '. $token,
						'Content-Type: application/octet-stream',
						'Dropbox-API-Arg: '. json_encode (array(
						'path' => '/'.joli_repertoire($repertoire).$fichier,
							),
						)
					);
	$datas =  json_encode (array(
				'path' => '/'.joli_repertoire($repertoire).$fichier,
				)
			);
	
	$ch = curl_init($url);
	
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_VERBOSE,1); // debug

	$recup = curl_exec($ch);
	
	curl_close($ch);    
	
	$res['statut'] = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	
	if ($res['statut'] != '200'){
		$res ['resultat'] = null;
		$res['erreur'] = $recup;
		return $res;
	}
	
	$download = fopen($nom_fichier, 'w');
	fputs($download, $recup);
	fclose($download);	
			
	$res ['resultat'] = null;
	
	return  $res;
}


function dropbox_api_upload($repertoire,$fichier) {
	
	// API: Upload un fichier Dropbox

	$action = 'exporter';
	$res = dropbox_demander_token($action);
	
	if ($res['statut']!='200'){
		$res ['resultat'] = null;
		return $res;
	}
	
	$token = $res['token'];
		
	$nom_fichier=$repertoire.$fichier;
	
	$upload = fopen($nom_fichier, 'r');
	$taille_upload = filesize($nom_fichier);
	
	$url = 'https://content.dropboxapi.com/2/files/upload';
	
	$headers = array (	'Authorization: Bearer '. $token,
						'Content-Type: application/octet-stream',
						'Dropbox-API-Arg: '. json_encode (array(
							'autorename' => false,
							'mode' => 'add',
							'mute' => false,
							'path' => '/'.joli_repertoire($repertoire).$fichier,
							'strict_conflict' => false,
							),
						)
					);
					
	$ch = curl_init($url);
	
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, fread($upload, $taille_upload));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_VERBOSE,1); // debug

	$recup = curl_exec($ch);
	
	curl_close($ch);    
	
	$res['statut'] = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	
	if ($res['statut'] != '200'){
		$res ['resultat'] = null;
		$res['erreur'] = $recup;
		return $res;
	}
			
	$res ['resultat'] = $recup;
	
	return  $res;
}

function dropbox_api_delete($repertoire,$fichier) {
	
	// API: Suppression d'un fichier Dropbox
	
	$action = 'exporter';
	$res = dropbox_demander_token($action);
	
	if ($res['statut']!='200'){
		$res ['resultat'] = null;
		return $res;
	}
	
	$token = $res['token'];

	$url = 'https://api.dropboxapi.com/2/files/delete_v2';
	
	$headers = array (	'Authorization: Bearer '. $token,
						'Content-Type: application/json',
							);
	$datas =  json_encode (array(
				'path' => '/'.joli_repertoire($repertoire).$fichier,
				)
			);
					
	$ch = curl_init($url);
	
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $datas);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_VERBOSE, 1); // debug

	$recup = curl_exec($ch);

	curl_close($ch);    
			
	$res['statut'] = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	
	if ($res['statut'] != '200'){
		$res['erreur'] = $recup;
		$res ['resultat'] = null;
		return $res;
	}
		
	$res['resultat']=$recup;
	
	return $res;
	
}

function dropbox_api_list_folder($action,$path) {
	
	// API: Liste des fichiers d'un dossier Dropbox
	
	$res = dropbox_demander_token($action);
	
	if ($res['statut']!='200'){
		$res ['resultat'] = null;
		return $res;
	}
	
	$token = $res['token'];

	$url = 'https://api.dropboxapi.com/2/files/list_folder';
	
	$headers = array (	'Authorization: Bearer '. $token,
						'Content-Type: application/json',
							);
	$datas =  json_encode (array(
				'include_deleted' => false,
				'include_has_explicit_shared_members' => false,
				'include_media_info' => true,
				'include_mounted_folders' => true,
				'include_non_downloadable_files' => false,
				'path' => $path,
				'recursive' => false,
				)
			);
					
	$ch = curl_init($url);
	
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $datas);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_VERBOSE, 1); // debug

	$recup = curl_exec($ch);

	curl_close($ch);    
			
	$res['statut'] = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	
	if ($res['statut'] != '200'){
		$res['erreur'] = $recup;
		$res ['resultat'] = null;
		return $res;
	}
		
	$res['resultat']=$recup;
	
	return $res;
	
}


function dropbox_demander_autorisation($action) {
	
	// Récupération du code au retour de l'API (GET) d'authorisation 
	// et sauvegarde en session (il peut reservir pendant un temps limité)
	
	if (($code =_request('code'))&&(session_get('dropbox_attente_code'))) {
			session_set('dropbox_code', $code);
			session_set('dropbox_attente_code',null);
			return $code;
	}
	
	// Récupération des client_id dans le paramétrage du plugin
	// Appel systématique de l'API avec un type d'accès en "offline" pour récupérer une autorisation longue durée lors des prochains accès
	
	$client_id = lire_config('dropbox/client_id');
	
	// Appel de l'API d'Autorisation en GET pour récupération du code
   
	$url = 'https://www.dropbox.com/oauth2/authorize';
	$url = parametre_url($url, 'client_id', $client_id, '&');
	$url = parametre_url($url, 'response_type', 'code', '&');
	$url = parametre_url($url, 'token_access_type', 'offline', '&');
	$url = parametre_url($url, 'redirect_uri', $GLOBALS['meta']['adresse_site'].'/ecrire/?exec=dropbox_'.$action,'&');

	include_spip('inc/headers');
	
	session_set('dropbox_attente_code', true);
	redirige_par_entete($url);
	
	exit;

}

function dropbox_token_offline($refresh_token) {
	
	// Récupération des client_id et client_secret dans le paramétrage du plugin
	
	$client_id = lire_config('dropbox/client_id');
	$client_secret = lire_config('dropbox/client_secret');
	
	$res['token'] = null;


	// Récupération du token offline
	
	$url = 'https://api.dropboxapi.com/oauth2/token';
	
	$options = array(
			'method' => 'POST',
			'datas' =>  array(
            'grant_type' => 'refresh_token',
			'refresh_token' => $refresh_token,
            'client_id' => $client_id,   
            'client_secret' => $client_secret,
			)       
		);
		
	include_spip('inc/distant');
	$recup = recuperer_url( $url, $options );
	

	if (isset($recup['status']))	{
		$res['statut'] = $recup['status'];
	}else{
		$res['statut'] = '999';
	}						   

	if (isset($recup['page']))	{
		$page=(json_decode($recup['page'],true));
	}else{
		$res['page'] = null;
	}

		
	// Capture des erreurs
	
	if ($res['statut'] != '200') {
		if (isset($page['error'])){
			$res['erreur'] = $page['error'];
		
			if (isset($page['error_description'])){
					$res['erreur'] .= ' '.$page['error_description'];
			}
		}
	} else {
		$res['erreur']=null;
	
	}
	
	// Récupération du token
	if (isset($page['access_token'])){	
		$res['token'] = $page['access_token'];
	}
	
	return $res;
}

function dropbox_token_online($code,$action='exporter') {
	
	// Récupération des client_id et client_secret dans le paramétrage du plugin	
	$client_id = lire_config('dropbox/client_id');
	$client_secret = lire_config('dropbox/client_secret');
	
	$res['refresh_token'];
	
	// Récupération du token online
	$url = 'https://api.dropboxapi.com/oauth2/token';
	
	$options = array(
			'method' => 'POST',
			'datas' =>  array(
            'grant_type' => 'authorization_code',
			'code' => $code,
            'client_id' => $client_id,                             
            'client_secret' => $client_secret,
			'redirect_uri' => $GLOBALS['meta']['adresse_site'].'/ecrire/?exec=dropbox_'.$action,
			)
		);
		
	include_spip('inc/distant');

	$recup = recuperer_url( $url, $options );
	
	if (isset($recup['statut']))	{
		$res['statut'] = $recup['status'];
	}else{
		$res['statut'] = '999';
	}

		if (isset($recup['status']))	{
		$res['statut'] = $recup['status'];
	}else{
		$res['statut'] = '999';
	}

	if ($recup['page']){
		$page=(json_decode($recup['page'],true));
	}
	
	// Capture des erreurs
	if ($res['statut'] != '200') {
		if (isset($page['error'])){
			$res['erreur'] = $page['error'];
		
			if (isset($page['error_description'])){
					$res['erreur'] .= ' '.$page['error_description'];
			}
		}
	} else {
		$res['erreur']=null;
	
	}
	
	// Récupération du token
	if (isset($page['access_token'])){
		$res['token'] = $page['access_token'];
	}
	
	// Récupération d'un refresh_token pour un prochain accès sans demande d'autorisation
	if (isset($page['refresh_token'])){
		$res['refresh_token'] = $page['refresh_token'];
	}
	
	return $res;
}

function dropbox_demander_token($action) {
	
	// Si le site distant est indisponible ou bien si les codes identifiant/mot de passe sont erronés, on renvoie de suite une erreur
	$recup['statut']= dropbox_test_app();
	if (isset($recup['statut'])
	AND $recup['statut'] != '200'){
		$recup['erreur']= _T('dropbox:info_check_ko');
		$recup['resultat'] = null;
		return $recup;
	}
	
	// Si demande de réinitialisation du jeton, on supprime le refresh token de la configuration
	if (lire_config('dropbox/init_token')){
		ecrire_config('dropbox/refresh_token', null);
		ecrire_config('dropbox/init_token', null);
	}
	
	// Si il existe déjà un refresh token en configuration alors on l'utilise
	if (lire_config('dropbox/refresh_token')){
		$refresh_token = lire_config('dropbox/refresh_token');
		$recup=dropbox_token_offline($refresh_token);
		
		// Si le token est refusé, c'est peut-être parce que le refresh token a expiré.
		// On fait une demande d'autorisation puis un nouveau token		
		if ($recup['statut']!='200'){
			$code = dropbox_demander_autorisation($action);
			$recup=dropbox_token_online($code,$action);
		}
	} else {
		//Si il n'existe pas encore de refresh token alors on fait une première demande d'autorisation puis un token
		$code = dropbox_demander_autorisation($action);
		$recup=dropbox_token_online($code,$action);
	}

	// Sauvegarde en paramètre du refresh_token s'il y en a un pour la prochaine fois
	if (isset($recup['refresh_token'])) {
		ecrire_config('dropbox/refresh_token', $recup['refresh_token']);
	}
		
	return $recup;
}

function dropbox_nettoyer ($options = []) {
	 // Fonction de nettoyage appelée par le cron SPIP

	// Vérification de la disponibilité de l'API
	if (dropbox_test_app() != '200') {
		spip_log(_T("dropbox:erreur_api"),'dropbox.'._LOG_ERREUR);
		return 0;
	}
	
	// Durée de conservation par défaut de 30 jours
	$duree = (lire_config('dropbox/duree', 30));
	
	// Nombre minimum de sauvegardes à conserver
	$nbr_garder = intval(lire_config('dropbox/nbr_garder'));

	
	$limite = date_add(date_create('now'),date_interval_create_from_date_string("-".$duree." days"));
	spip_log(_T('dropbox:lancement_nettoyage').' '.date_format($limite,"d-m-Y"),'dropbox.'._LOG_INFO_IMPORTANTE);

	$duree_sauvegarde = intval( $duree);
	if ($duree_sauvegarde > 0) {
		// Stocker la date actuelle pour calculer l'obsolescence
		$temps = time();

		// Liste des exports Dropbox existants
		include_spip('inc/dump');
		$dir_dump=dump_repertoire();
		$path='/'.joli_repertoire($dir_dump);
	
		$res=dropbox_api_list_folder(null,$path);
		$resultat = json_decode($res['resultat'],true);
		$entries = $resultat['entries'];
		$nbr_sauvegardes = 0;
		
		// Filtre "contient..." des exports
		if ($critere = lire_config('dropbox/critere')){
			$pattern = "/.*{$critere}.*\.(zip|sql|sqlite)$/i"; //$pattern = "/^.{$critere}.\.(zip|sql|sqlite)$";
		}else{ $pattern = "/.*\.(zip|sql|sqlite)$/i";
		}
		
		// Tri des sauvegardes par date serveur descendante
		usort($entries,'dropbox_tri_desc');
		
		// Suppression des exports
		foreach ($entries as $_sauvegarde) {		
			$match = preg_match($pattern,$_sauvegarde['name']) ?? null;
			if ($match) {				
				// Nombre minimum de sauvegardes
				$nbr_sauvegardes += 1;
				if ($_sauvegarde['.tag']== "file" && $nbr_garder AND $nbr_sauvegardes > $nbr_garder) {
				$date_fichier = strtotime($_sauvegarde['server_modified']);
				
				// Suppression des sauvegardes obsolètes
				if ($temps > ($date_fichier + $duree_sauvegarde * 3600 * 24)) {
					dropbox_api_delete($dir_dump,$_sauvegarde['name']);
					$liste[] = $_sauvegarde['name'];
					}
				}
			
			}
		}
		// détail des sauvegardes supprimées
		if (isset($liste)) {
			$detail = "\n - ".join("\n - ",$liste);
			spip_log(_T("dropbox:details_nettoyage").$detail,'dropbox.'._LOG_INFO_IMPORTANTE);
			
			// Notification si demandé dans la configuration
			if(lire_config('dropbox/notification')){
			
					// Construction du sujet du mail
					include_spip('inc/texte');
					$base = $GLOBALS['connexions'][0]['db'];
					$sujet = "[" . typo($GLOBALS['meta']['nom_site'])
							. "][Dropbox] "
							. _T('dropbox:message_nettoyer_sujet');
					
				// Construction du message
					$message = _T('dropbox:info_message_nettoyer').$detail;
		
					// Détermination de la liste des destinataires
					$mails = lire_config('dropbox/mails');
					$tous = ($mails) ? explode(',', $mails) : array();
					// Ajout du webmaster
					$tous[] = $GLOBALS['meta']['email_webmaster'];
			
					// Passage par le pipeline "notifications_destinataires"
					$destinataires = pipeline('notifications_destinataires',
								array(
									'args'=>array('quoi'=>'dropbox','id'=>'','options'=>''),
									'data'=>$tous)
								);
							
					// Envoi des emails
					$envoyer_mail = charger_fonction('envoyer_mail','inc');
					foreach($destinataires as $email){
						$envoyer_mail($email, $sujet, $message, '', '');
					}
			}
		}
	}
		
	
	
	// Fin du nettoyage automatique
	if (isset($liste)) {
		$nombre = count($liste);
	}else{
		$nombre = 0;
	}
	spip_log(_T("dropbox:info_nettoyage_termine",['nombre' => $nombre]),'dropbox.'._LOG_INFO_IMPORTANTE);

	return 1;
}

function dropbox_tri_desc($a, $b){
    return $a['server_modified'] < $b['server_modified'];
}
# Changelog


## 1.3.0 - 2024-10-24

### Feat

- Nettoyage automatique des exports Dropbox distants


## 1.2.1 - 2024-09-04

### Fix

- Branchement sur plugin dumpauto en utilisant le pipeline post_sauvegarde pour automatisation de l'export

## 1.2.0 - 2024-07-05

### Build

- Compatibilité 4.*

## 1.1.3 - 2024-05-27

### Fix

- Ajout d'un include des fonctions Dropbox dans le fichier pipelines pour éviter un plantage en mode cron

## 1.1.2 - 2024-05-27

### Feat

- Branchement sur plugin mes_fichiers en utilisant le pipeline post_sauvegarde pour automatisation de l'export


## 1.1.0 - 2024-03-03

### Feat

- Branchement sur plugin saveauto en utilisant le pipeline post_sauvegarde pour automatisation de l'export
- Message en fin de boucle DATA si aucune ligne ramenée

### Fix

- fix: Tester la disponibilité du site distant avant demande de jeton pour éviter plantage

## 1.0.1 - 2024-01-30

### Feat

- Réinitialisation possible du jeton "offline" dans la configuration

### Fix

- Demande systématique d'un jeton "offline" lors de la demande d'autorisation


## 1.0.0 - 2024-01-15

### Feat

- initialisation du plugin Dropbox


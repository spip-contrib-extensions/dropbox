<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = [

	// D
	'dropbox_description' => 'Sauvegarde du dump dans Dropbox',
	'dropbox_nom' => 'Dropbox',
	'dropbox_slogan' => 'Mettre son SIP en boite',
];

# Dropbox pour SPIP

Export (et import) des sauvegardes SPIP sqlite dans (depuis) son espace personnel Dropbox

## Documentation

https://contrib.spip.net/Dropbox-5527


## ToDo

- Génériquer le plugin pour accepter d'autres hébergements distants 
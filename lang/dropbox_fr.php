<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = [

	// D
	'dropbox_titre' => 'Dropbox',
	'dropbox_titre_exporter' => 'Exporter Dropbox',
	'dropbox_titre_importer' => 'Importer Dropbox',
	
	// B
	'bouton_exporter_dropbox' => 'Exporter vers Dropbox',
	'bouton_importer_dropbox' => 'Importer depuis Dropbox',
	
	// C
	'confirmer_supprimer_export' => 'Êtes-vous sûr de vouloir supprimer cet export ?',
	'cfg_exemple' => 'Exemple',
	'cfg_exemple_explication' => 'Explication de cet exemple',
	'cfg_titre_parametrages' => 'Paramétrages',
	'confirmer_exporter_base' => 'Oui, je veux exporter ma sauvegarde vers Dropbox',
	'confirmer_importer_base' => 'Oui, je veux importer ma sauvegarde depuis Dropbox',
	
	// E
	'explication_client_id' => 'Identifiant client Dropbox',
	'explication_client_secret' => 'Code secret client Dropbox',
	'explication_init_token' => 'Rénitialiser le jeton, notamment en cas de modification a posteriori des droits sur Dropbox. Il vous sera alors demandé une nouvelle autorisation.',
	'explication_export_saveauto' => 'Automatisation de l\'export vers Dropbox après sauvegarde du plugin Saveauto',
	'explication_notif_saveauto' => 'Si vous souhaitez être prévenus des exports automatiques des sauvegardes Saveauto, activez les notifications. Les destinataires seront ceux paramétrés dans le plugin Saveauto.',
	'explication_export_mes_fichiers' => 'Automatisation de l\'export vers Dropbox après sauvegarde du plugin Mes_fichiers',
	'explication_notif_mes_fichiers' => 'Si vous souhaitez être prévenus des exports automatiques des sauvegardes Mes_fichiers, activez les notifications. Les destinataires seront ceux paramétrés dans le plugin Mes_fichiers.',
	'explication_export_dumpauto' => 'Automatisation de l\'export vers Dropbox après sauvegarde du plugin Dumpauto',
	'explication_notif_dumpauto' => 'Si vous souhaitez être prévenus des exports automatiques des sauvegardes Dumpauto, activez les notifications. Les destinataires seront ceux paramétrés dans le plugin Dumpauto.',
	'explication_nettoyage' => 'Activer le nettoyage journalier des exports Dropbox',
	'explication_duree' => 'Saisir la durée de conservation des exports Dropbox en jours',
	'explication_nbr_garder' => 'Saisir le nombre minimum des exports Dropbox à conserver',
	'explication_notification' => 'Si vous souhaitez être prévenu des nettoyages automatiques, activez les notifications',
	'explication_mails' => 'Saisir les adresses emails des destinataires en les séparant par des virgules ",". Ces adresses s\'ajoutent à celle du webmestre du site',
	'explication_critere' => 'Si différents types d\'exports sont présents dans votre répertoire Dropbox, vous pouvez filtrer les noms par le critère "Contient...". Par défaut, tous les exports présents sont susceptibles d\'être nettoyés.',
	'exporter' => 'Exporter',
	'exports_existants' => 'Exports Dropbox existants',
	'erreur_api' => 'Erreur d\'accès à l\'API Dropbox. Vérifier l\'identifiant, le mot de passe ou la connexion',

	// I
	'importer' => 'Importer',
	'info_aucun_fichier_trouve' => 'Aucun fichier n\'a été trouvé',
	'info_taille_max_fichier' => 'La taille du fichier est supérieure à 150 Mo',
	'info_selection_export' => 'Vous avez choisi d\'exporter la sauvegarde @fichier@.',
	'info_selection_import' => 'Vous avez choisi d\'importer la sauvegarde @fichier@.',
	'info_export_ok' => 'Le fichier @fichier@ a été exporté.',
	'info_export_ko' => 'Le fichier @fichier@ n\'a pas pu être exporté.',
	'info_import_ok' => 'Le fichier @fichier@ a été importé.',
	'info_check_ok' => 'Les identifiant et code secret Dropbox sont valides.',
	'info_check_ko' => 'Les identifiant et code secret Dropbox sont invalides.',
	'info_message_nettoyer' => 'Exports(s) Dropbox supprimé(s):',
	'info_nettoyage_termine' => 'Nettoyage automatique terminé: @nombre@ export(s) Dropbox supprimé(s)',

	// L
	
	'legende_parametrage' => 'Paramétrage Dropbox pour SPIP',
	'legende_saveauto' => 'Paramétrage Dropbox pour plugin Saveauto',
	'legende_mes_fichiers' => 'Paramétrage Dropbox pour plugin Mes_fichiers',
	'legende_dumpauto' => 'Paramétrage Dropbox pour plugin Dumpauto',
	'legende_nettoyage' => 'Paramétrage du nettoyage des exports Dropbox',
	'label_client_id' => 'Client_id',
	'label_client_secret' => 'Client_secret',
	'label_init_token' => 'Réinitialisation du jeton',
	'label_export_saveauto' => 'Export automatique des sauvegardes Saveauto',
	'label_notif_saveauto' => 'Notification des exports automatiques des sauvegardes Saveauto',
	'label_notif_saveauto' => 'Notification des exports Saveauto',
	'label_export_mes_fichiers' => 'Export automatique des sauvegardes Mes_fichiers',
	'label_notif_mes_fichiers' => 'Notification des exports automatiques des sauvegardes Mes_fichiers',
	'label_notif_mes_fichiers' => 'Notification des exports Mes_fichiers',
	'label_export_dumpauto' => 'Export automatique des sauvegardes Dumpauto',
	'label_notif_dumpauto' => 'Notification des exports automatiques des sauvegardes Dumpauto',
	'label_notif_dumpauto' => 'Notification des exports Dumpauto',
	'label_nom_fichier_export' => 'Nom du fichier pour l\'export',
	'label_nom_fichier_import' => 'Nom du fichier pour l\'import',
	'label_nettoyage' => 'Nettoyage journalier',
	'label_duree' => 'Durée',
	'label_nbr_garder' => 'Minimum',
	'label_notification' => 'Notifications',
	'label_mails' => 'Adresses email',
	'label_critere' => '"Nom contient..."',
	'lancement_nettoyage' => 'Lancement automatique du nettoyage des exports Dropbox antérieurs au',
	
	// M
	'message_export_sujet' => 'Export de la base @base@',
	'message_nettoyer_sujet' => 'Nettoyage exports Dropbox',


	// T
	'titre_page_configurer_dropbox' => 'Configuration Dropbox',
	'titre_page_exporter_dropbox' => 'Exporter vers Dropbox',
	'titre_page_importer_dropbox' => 'Importer depuis Dropbox',
	'texte_init_token' => 'Réinitialisation du jeton',
	'texte_export_saveauto' => 'Exporter automatiquement les sauvegardes Saveauto',
	'texte_notif_saveauto' => 'Notifier les exports automatiques des sauvegardes Saveauto',
	'texte_export_mes_fichiers' => 'Exporter automatiquement les sauvegardes Mes_fichiers',
	'texte_notif_mes_fichiers' => 'Notifier les exports automatiques des sauvegardes Mes_fichiers',
	'texte_export_dumpauto' => 'Exporter automatiquement les sauvegardes Dumpauto',
	'texte_notif_dumpauto' => 'Notifier les exports automatiques des sauvegardes Dumpauto',
	'texte_nettoyage' => 'Activer le nettoyage journalier des sauvegardes',
	'texte_notification' => 'Activer les notifications de sauvegarde et de nettoyage',

];
